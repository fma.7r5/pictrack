package com.ricardoarreola.pictrack;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


import static java.lang.Math.round;

public class MainActivity extends AppCompatActivity {


    private static File StorageDir;
    public boolean stopTimer = false;
    private Camera mCamera;
    private CameraPreview mPreview;

    public Button startButton, stopButton;
    public ProgressBar progressBar, progressBar2;
    public TextView progressLabel, timeLabel, qntyLabel;
    public EditText timeText, qntyText;
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = findViewById(R.id.btn_start);
        stopButton = findViewById(R.id.btn_stop);
        progressBar = findViewById(R.id.progressBar);
        progressBar2 = findViewById(R.id.progressBar2);
        progressLabel = findViewById(R.id.lbl_progress);
        timeText = findViewById(R.id.tf_time);
        qntyText = findViewById(R.id.tf_qnty);
        timeLabel = findViewById(R.id.lbl_time);
        qntyLabel = findViewById(R.id.lbl_qnty);


        InitialState();

        startButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onClick(View v) {
                if (ValidateFields()) {
                    ExecutePreview();
                    SetInputFields(View.GONE);
                    StartPictures();
                }
            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopTimer = true;
            }
        });

    }

    private void SetInputFields(int viw) {
        timeText.setVisibility(viw);
        timeLabel.setVisibility(viw);
        qntyText.setVisibility(viw);
        qntyLabel.setVisibility(viw);
    }

    private boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                //Log.v(TAG,"Permission is granted");
                return true;
            } else {
                //Log.v(TAG,"Permission is revoked");
                //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Log.v(TAG,"Permission is granted");
                return true;
            } else {

                //Log.v(TAG,"Permission is revoked");
                //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            System.out.println(e);
        }
        return c;
    }

    private boolean ValidateFields() {
        String messageTime = timeText.getText().toString();
        String messageQnty = qntyText.getText().toString();

        boolean validateQnty = true;
        boolean validateTime = true;


        if (messageTime.length() == 0) {
            timeText.setError("Introduce el tiempo de minutos!");
            validateQnty = false;
        }
        if (messageQnty.length() == 0) {
            qntyText.setError("Introduce la cantidad de fotos!");
            validateQnty = false;
        }
        return validateTime && validateQnty;
    }


    private void ExecutePreview() {
        MainActivity g = this;
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mCamera = getCameraInstance();
                mPreview = new CameraPreview(g, mCamera);
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.addView(mPreview);
            }
        });

    }

    private void InitialState() {
        String[] PERMISSIONS = {
                android.Manifest.permission.READ_CONTACTS,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.VIBRATE,
                android.Manifest.permission.CAMERA
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_MULTIPLE_REQUEST);
        }

        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.GONE);
        progressLabel.setVisibility(View.GONE);

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST: {
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per;

                        }

                    }
                    // Show permissionsDenied
                }
                return;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void StartPictures() {
        startButton.setVisibility(View.INVISIBLE);
        stopButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressLabel.setVisibility(View.VISIBLE);

        int number = Integer.parseInt(qntyText.getText().toString());
        int time = Integer.parseInt(timeText.getText().toString()) * 60;
        progressBar.setMax(number);
        progressBar2.setMax((time / number) - 1);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int n = 0;
            final int frequency = time / number;

            @SuppressLint("DefaultLocale")
            @Override
            public void run() {
                n += 1;
                if (n > time || stopTimer) {
                    timer.cancel();
                    try {
                        synchronized (this) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    n = 0;
                                    stopTimer = false;
                                    RestartCounter();
                                    SetInputFields(View.VISIBLE);
                                    openFolder();

                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    long module = n % frequency;
                    progressBar2.setProgress(round(module), true);
                    if ((int) module == (frequency - 4 % frequency)) {
                        VibrateAfterPic(100);
                    }
                    if ((int) module == (frequency - 3 % frequency)) {
                        VibrateAfterPic(100);
                    }
                    if ((int) module == (frequency - 2 % frequency)) {
                        VibrateAfterPic(400);
                    }
                    if ((int) module == 0) {
                        int roundValue = round(n / frequency);
                        progressLabel.setText(String.format("%d/%d", roundValue, number));
                        System.out.println(String.format("%d/%d", roundValue, number));
                        TakePic();
                        progressBar.setProgress(roundValue, true);
                        progressBar2.setProgress(0);
                    }
                }
            }
        }, 1000, 1000);

    }

    @SuppressLint("MissingPermission")
    private void VibrateAfterPic(int millis) {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(millis);
        }
    }

    public void openFolder() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/Pictures/");
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setDataAndType(selectedUri, "*/*");
        startActivity(intent);
    }

    private void RestartCounter() {
        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.INVISIBLE);
    }

    public void TakePic() {
        mCamera.takePicture(null, null, mPicture);
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        System.out.println("CREANDO OUTPUT");
        String todayDateStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PicTr_" + todayDateStamp);
        StorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        System.out.println("File path creado:" + mediaStorageDir.getAbsolutePath().toString());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            System.out.println("No existe el path");
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("PicTr_(DATE)" + "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("MM-dd_HH:mm:ss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            System.out.println("TTTTTTTTTTTTTTTTTTTTTT taking pic TTTTTTTTTTTTTTTTTTTT");
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                System.out.println("Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("File not found: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("Error accessing file: " + e.getMessage());
            }
            mCamera.startPreview();
        }
    };

}

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            System.out.println("Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
            mCamera.setDisplayOrientation(90);

        } catch (Exception e) {
            System.out.println("Error starting camera preview: " + e.getMessage());
        }
    }
}


